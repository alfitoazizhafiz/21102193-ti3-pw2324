<!DOCTYPE html>
<html>

<head>
    @include('layouts.head')
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        {{-- header --}}
        @include('layouts.header')
        {{-- end header --}}
        <!-- Left side column. contains the logo and sidebar -->

        {{-- sidebar --}}
        @include('layouts.leftsidebar')
        {{-- end sidebar --}}

        <!-- Content -->
        @include('layouts.content')
        <!-- end content  -->

        {{-- footer  --}}
        @include('layouts.footer')
        {{-- end footer  --}}

        <!-- Control Sidebar -->
        @include('layouts.controlSidebar')
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    @include('layouts.script')
</body>

</html>

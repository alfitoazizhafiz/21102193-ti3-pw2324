<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;


class StudentController extends Controller
{
    //
    public function create()
    {
        return view('student.create');
    }
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:students',
            'nama' => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:P,L',
            'jurusan' => 'required',
            'alamat' => '',
            'image' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg|max:5120 ',
        ]);
        $mahasiswa = new Student();
        $mahasiswa->nim = $validateData['nim'];
        $mahasiswa->name = $validateData['nama'];
        $mahasiswa->gender = $validateData['jenis_kelamin'];
        $mahasiswa->departement = $validateData['jurusan'];
        $mahasiswa->address = $validateData['alamat'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move(public_path('assets/images'), $namaFile);
            $mahasiswa->image = $namaFile;
        }
        $mahasiswa->save();
        // $request->Session()->flush('pesan', 'Penambahan data berhasil');
        return redirect()->route('student.index')->with('Penambahan data berhasil');
    }
    public function index()
    {
        $mahasiswas = Student::all();
        return view('student.index', ['students' => $mahasiswas]);
    }
    public function show($student_id)
    {
        $result = Student::findOrFail($student_id);
        return view('student.show', ['student' => $result]);
    }
    public function edit($student_id)
    {
        $result = Student::findOrFail($student_id);
        return view('student.edit', ['student' => $result]);
    }
    public function update(Request $request, Student $student)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:students',
            'nama' => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:P,L',
            'jurusan' => 'required',
            'alamat' => '',
            'image' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg|max:5120 ',
        ]);
        $student->nim = $validateData['nim'];
        $student->name = $validateData['nama'];
        $student->gender = $validateData['jenis_kelamin'];
        $student->departement = $validateData['jurusan'];
        $student->address = $validateData['alamat'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move(public_path('assets/images'), $namaFile);
            $student->image = $namaFile;
        }
        $student->save();
        // $request->Session()->flush('pesan', 'Perubahan data' + $student->name + 'berhasil');
        return redirect()->route('student.show', ['student' => $student->id])->with('pesan', 'Perubahan data berhasil');
    }
    public function destroy(Request $request, Student $student)
    {
        $student->delete();
        // $request->Session()->flush('pesan', 'Hapus data berhasil');
        return redirect()->route('student.index')->with('pesan', 'Hapus data berhasil');
    }
}
